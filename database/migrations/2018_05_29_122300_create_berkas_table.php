<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berkas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('deskripsi');
            $table->string('path');
            $table->string('tipefile');
            $table->string('thumbnail');
            $table->char('tampil', 1);
            $table->timestamps();
            $table->unsignedInteger('unitId');
            $table->foreign('unitId')
                  ->references('id')
                  ->on('unitkerja')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('berkas');
    }
}
