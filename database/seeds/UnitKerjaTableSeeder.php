<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UnitKerjaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unitkerja')->insert([
            'nama' => 'Administrator',
            'email' => 'admin@bps.go.id',
            'password' => bcrypt('admin'),
            'deskripsi' => 'User Account Administrator',
            'level' => 'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'poin' => 0
        ]);
    }
}
