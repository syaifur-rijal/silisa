<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Unitkerja extends Authenticatable
{

    protected $table = "unitkerja";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'email', 'password', 'level', 'deskripsi'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'poin'
    ];
}
