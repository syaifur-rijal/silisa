<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Unitkerja;

use Auth;
use DB;

class MemberController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {               
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $anggota =  Unitkerja::all();

        $class = 'Member';

        return view('pages.content.member.view', compact(
            'class',
            'anggota',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $class = 'Member';

        return view('pages.content.member.create', compact(
            'class',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Unitkerja::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'deskripsi' => $request->deskripsi,
            'level' => $request->level
        ]);

        return redirect('member');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2);

        $anggota = Unitkerja::findOrFail($id);

        $class = 'Member';

        return view('pages.content.member.edit', compact(
            'class',
            'anggota',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Unitkerja::findOrFail($id)->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'deskripsi' => $request->deskripsi,
            'level' => $request->level
        ]);

        return redirect('member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Unitkerja::findOrFail($id)->delete();

        return redirect('member');
    }
}
