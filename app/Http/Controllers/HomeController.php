<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

Use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Query all content
         */

        $cGambar1 = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('tampil', '1')
                ->count();
        
        $cBooklet1 = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('tampil', '1')
                ->count();

        $cSlide1 = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('tampil', '1')
                ->count();

        $cVideo1 = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('tampil', '1')
                ->count();

        $cInfografis1 = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('tampil', '1')
                ->count();

        $cLain1 = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('tampil', '1')
                ->count();
               
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 
        
        $class = 'Dashboard';
        
        return view('home', compact(
                'class',        
                'hasil', 
                'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo', 
                'cGambar1', 'cBooklet1', 'cLain1', 'cInfografis1', 'cSlide1', 'cVideo1'
        ));
    }
}
