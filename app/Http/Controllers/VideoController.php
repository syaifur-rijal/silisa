<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Berkas;

use Auth;
use DB;
use File;

class VideoController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $video = DB::table('berkas')
                    ->join('unitkerja', 'berkas.unitId', '=', 'unitkerja.id')
                    ->select('berkas.*', 'unitkerja.nama as username', 'unitkerja.level')
                    ->where('tipefile', 'video')
                    ->where('tampil', '1')
                    ->orderBy('berkas.updated_at', 'desc')
                    ->get();

        $class = 'Video';

        return view('pages.content.video.view', compact(
            'class',
            'video',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path = public_path();

        $file = Berkas::where('id', $id)->get();

        File::delete($path . "/thumb/" . $file[0]->thumbnail);
        File::delete($path . "/berkas/" . $file[0]->path);

        Berkas::findOrFail($id)->delete();

        return redirect('video');
    }
}
