<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Berkas;

use Auth;
use DB;
use File;

class RepositoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $repositori = Berkas::where('unitId', Auth::user()->id)->orderBy('updated_at', 'desc')->get();
    
        $class = 'Berkasku';

        return view('pages.content.repository.view', compact(
            'class',
            'repositori',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $class = 'Berkasku';

        return view('pages.content.repository.create', compact(
            'class',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'deskripsi' => 'required|max:255',
            'tipefile' => 'required',
            'thumbnail' => 'required|mimes:jpeg,jpg,png|max:300',
            'berkas' => 'required|mimes:jpeg,png,doc,docx,ppt,pptx,pdf,mp4|max:10000'
        ]);

        $path = public_path();

        $temp0 = $request->file('thumbnail');
        $temp1 = $request->file('berkas');
          
        $thumb = "thumb" . "_" . Auth::user()->id . "_" . $request->nama . "_" . time() . "." . $temp0->getClientOriginalExtension();
        $berkas = "berkas" . "_" . Auth::user()->id . "_" . $request->nama . "_" . time() . "." . $temp1->getClientOriginalExtension();
        
        $temp0->move($path . "/thumb", $thumb);
        $temp1->move($path . "/berkas", $berkas);
        
        Berkas::create([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'tipefile' => $request->tipefile,
            'thumbnail' => $thumb,
            'path' => $berkas,
            'tampil' => '0',
            'unitId' => Auth::user()->id
        ]);

        return redirect('repository');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * Query by Id User
         */

        $cGambar = DB::table('berkas')
                ->where('tipefile', 'gambar')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $cBooklet = DB::table('berkas')
                ->where('tipefile', 'booklet')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cSlide = DB::table('berkas')
                ->where('tipefile', 'slide')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cInfografis = DB::table('berkas')
                ->where('tipefile', 'infografis')
                ->where('unitId', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cVideo = DB::table('berkas')
                ->where('tipefile', 'video')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();

        $cLain = DB::table('berkas')
                ->where('tipefile', 'lain')
                ->where('unitID', Auth::user()->id)
                ->where('tampil', '1')
                ->count();
        
        $hasil = ($cGambar * 1) + ($cBooklet * 3) + ($cSlide * 2) + ($cVideo * 3) + ($cLain * 1) + ($cInfografis * 2); 

        $repositori = Berkas::findOrFail($id);

        $class = 'Berkasku';

        return view('pages.content.repository.edit', compact(
            'class',
            'repositori',
            'hasil',
            'cGambar', 'cBooklet', 'cLain', 'cInfografis', 'cSlide', 'cVideo'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = public_path();

        $file = Berkas::where('id', $id)->get();

        File::delete($path . "/thumb/" . $file[0]->thumbnail);
        File::delete($path . "/berkas/" . $file[0]->path);

        $this->validate($request, [
            'nama' => 'required|max:50',
            'deskripsi' => 'required|max:255',
            'tipefile' => 'required',
            'thumbnail' => 'required|mimes:jpeg,jpg,png|max:300',
            'berkas' => 'required|mimes:jpeg,png,doc,docx,ppt,pptx,pdf,mp4|max:10000'
        ]);

        $temp0 = $request->file('thumbnail');
        $temp1 = $request->file('berkas');
          
        $thumb = "thumb" . "_" . Auth::user()->id . "_" . $request->nama . "_" . time() . "." . $temp0->getClientOriginalExtension();
        $berkas = "berkas" . "_" . Auth::user()->id . "_" . $request->nama . "_" . time() . "." . $temp1->getClientOriginalExtension();
        
        $temp0->move($path . "/thumb", $thumb);
        $temp1->move($path . "/berkas", $berkas);

        Berkas::findOrFail($id)->update([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'tipefile' => $request->tipefile,
            'thumbnail' => $thumb,
            'path' => $berkas,
            'tampil' => '1',
            'unitId' => Auth::user()->id
        ]);

        return redirect('repository');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path = public_path();

        $file = Berkas::where('id', $id)->get();

        File::delete($path . "/thumb/" . $file[0]->thumbnail);
        File::delete($path . "/berkas/" . $file[0]->path);

        Berkas::findOrFail($id)->delete();

        return redirect('repository');
    }
}
