<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// route untuk otentikasi
Route::auth();

// jika user mengakses root document akan diarahkan ke /home
Route::get('/', function() {
    return redirect('/home');
});

Route::get('/register', function() {
    return redirect('/home');
});

// semua route di dalam grup hanya bisa diakses oleh user terotentikasi
Route::group(['middleware' => 'auth'], function() {
    Route::resource('/home', 'HomeController');
    Route::resource('/repository', 'RepositoryController');
    Route::resource('/picture', 'PictureController');
    Route::resource('/booklet', 'BookletController');
    Route::resource('/slide', 'SlideController');
    Route::resource('/infografis', 'InfografisController');
    Route::resource('/video', 'VideoController');
    Route::resource('/others', 'OthersController');
    Route::resource('/member', 'MemberController');
    Route::resource('/file', 'FileController');
    Route::resource('/profile', 'ProfilController');
});