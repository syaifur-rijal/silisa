@extends('layouts.app')

@section('title')
    <title>Dashboard</title>
@endsection

@section('assets')
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="{{ url('/css/morris.css') }}" type="text/css"/>   
    <!-- calendar -->
    <link rel="stylesheet" href="{{ url('css/monthly.css') }}">
    <!-- raphael js & morris js -->
    <script src="{{ url('/js/raphael-min.js') }}"></script>
    <script src="{{ url('/js/morris.js') }}"></script>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar', ['class' => $class])
    
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- //market-->
            <div class="market-updates">
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-1">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-picture-o"></i>
                        </div>    
                        <div class="col-lg-12 block-gambar">
                        <h4>Gambar</h4>
                        <p>
                            @if(!empty($cGambar1))
                                {{ $cGambar1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                        </p>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-2">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-book" ></i>
                        </div>
                        <div class="col-lg-12 block-gambar">
                        <h4>Booklet</h4>
                            <p>
                            @if(!empty($cBooklet1))
                                {{ $cBooklet1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                            </p>
                        </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-3">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-desktop"></i>
                        </div>
                        <div class="col-lg-12 block-gambar">
                            <h4>Paparan</h4>
                            <p>
                            @if(!empty($cSlide1))
                                {{ $cSlide1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                            </p>
                        </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-4">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="col-lg-12 block-gambar">
                            <h4>Infografis</h4>
                            <p>
                            @if(!empty($cSlide1))
                                {{ $cSlide1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                            </p>
                        </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-5">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-video-camera" aria-hidden="true"></i>
                        </div>
                        <div class="col-lg-12 block-gambar">
                            <h4>Video</h4>
                            <p>
                            @if(!empty($cVideo1))
                                {{ $cVideo1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                            </p>
                        </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-2 market-update-gd">
                    <div class="market-update-block clr-block-6">
                        <div class="col-lg-12 block-gambar-icon">
                            <i class="fa fa-certificate" aria-hidden="true"></i>
                        </div>
                        <div class="col-lg-12 block-gambar">
                            <h4>Lainnya</h4>
                            <p>
                            @if(!empty($cLain1))
                                {{ $cLain1 }} Konten
                            @else
                                {{ 0 }} Konten
                            @endif
                            </p>
                        </div>
                      <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>	
        </section>

        @include('partial.footer')
    </section>
    <!--main content end-->
</section>
@endsection