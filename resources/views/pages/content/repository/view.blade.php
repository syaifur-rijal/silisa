@extends('layouts.app')

@section('title')
<title>Berkas Saya</title>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar', ['class' => $class])

    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                <div class="box-add clearfix">
                    <a href="{{ url("/repository/create") }}" class="pull-left btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Tambah Berkas</a>
                </div>
                @if(!empty($repositori))
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Berkas</div>
                    <div>
                        <table class="table" ui-jq="footable" ui-options='{"paging": { "enabled": true },"filtering": { "enabled": true },"sorting": { "enabled": true }}'>
                            <thead>
                                <tr>
                                    <th data-breakpoints="xs">No</th>
                                    <th>Nama Berkas</th>
                                    <th>Keterangan</th>
                                    <th data-breakpoints="xs sm md">Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($repositori as $index => $posisi)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>
                                            <img src="{{ URL::to('/thumb/'.$posisi->thumbnail) }}">
                                            <p>{{ $posisi->nama }}</p>
                                        </td>
                                        <td>
                                            <div style="line-height:2.0;padding:0">
                                                <p><b>{{ $posisi->deskripsi }}</b></p>
                                                <p><b>Tipe berkas:</b> {{ $posisi->tipefile }}</p>
                                                <p><b>Tanggal dibuat:</b> {{ $posisi->created_at }}</p>
                                                <p><b>Tanggal diperbaharui:</b> {{$posisi->updated_at }}</p>
                                                <br>
                                                <a class="btn btn-info" href="{{ URL::to('/berkas/'.$posisi->path) }}"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Unduh</a>
                                            </div>
                                        </td>
                                        <td>
                                            {!! Form::model($posisi, ['method' => 'DELETE', 'action' => ['RepositoryController@destroy', $posisi->id]]) !!}
                                            <a href="{{ action('RepositoryController@edit', $posisi->id) }}" class="btn btn-warning fa fa-pencil-square-o"></a>
                                            <button type="submit" class="btn btn-danger fa fa-trash"></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>            
                    </div>
                </div>
                @else
                <div class="panel panel-default">
                    <h4 class="center">Maaf Data Tidak Ditemukan</h4>
                </div>
                @endif
            </div>
        </section>
        @include('partial.footer')
    </section>
</section>
@endsection