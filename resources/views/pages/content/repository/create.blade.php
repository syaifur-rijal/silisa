@extends('layouts.app')

@section('title')
<title>Tambah Berkas</title>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar')

    <section id="main-content">
        <section class="wrapper">
            <div class="form-w3layouts">
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">Unggah Berkas Baru</header>
                            <div class="panel-body">
                                <div class="position-center">
                                    {!! Form::open(['url' => '/repository', 'files' => 'true', 'class' => 'form-horizontal bucket-form', 'enctype' => 'multipart/form-data']) !!}
                                        <div class="form-group">
                                            {!! Form::label('labelNama', 'Nama Berkas') !!}
                                            {!! Form::text('nama', null, ['class' => 'form-control']) !!}                                                
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('labelDeskripsi', 'Deskripsi') !!}
                                            {!! Form::textarea('deskripsi', null, ['class' => 'form-control', 'rows' => '5']) !!}    
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('labelTipe', 'Tipe Berkas') !!}
                                            {!! Form::select('tipefile', ['gambar' => 'Gambar', 'booklet' => 'Booklet', 'slide' => 'Paparan/Slide', 'infografis' => 'Infografis', 'video' => 'Video', 'lain' => 'Lainnya'], null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('labelThumb', 'Pilih Thumbnail') !!}
                                            {!! Form::file('thumbnail') !!}
                                            <p class="help-block">Tipefile <b>JPG, JPEG</b> Ukuran gambar max. 300KB</p>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('labelBerkas', 'Pilih Berkas') !!}
                                            {!! Form::file('berkas') !!}
                                            <p class="help-block">Tipefile <b>doc, docx, ppt, pptx, pdf, png, jpeg, mp4</b> max. 10MB</p>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-cloud-upload"></i>&nbsp;Unggah</button>
                                        </div>
                                        @if($errors->any())
                                        <div class="form-group">    
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @endif
                                    {!! Form::close() !!}
                                </div>                          
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
        @include('partial.footer')
    </section>
</section>
@endsection