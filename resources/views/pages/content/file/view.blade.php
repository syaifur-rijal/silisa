@extends('layouts.app')

@section('title')
<title>Approve Konten</title>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar', ['class' => $class])

    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                @if(!empty($editberkas))
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Approval Konten</div>
                    <div>
                        <table class="table" ui-jq="footable" ui-options='{"paging": { "enabled": true },"filtering": { "enabled": true },"sorting": { "enabled": true }}'>
                            <thead>
                                <tr>
                                    <th data-breakpoints="xs">No</th>
                                    <th>Nama Berkas</th>
                                    <th>Keterangan</th>
                                    <th data-breakpoints="xs sm md">Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($editberkas as $index => $posisi)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>
                                            <img src="{{ URL::to('/thumb/'.$posisi->thumbnail) }}">
                                            <p>{{ $posisi->nama }}</p>
                                        </td>
                                        <td>
                                            <div style="line-height:2.0;padding:0">
                                                <p><b>{{ $posisi->deskripsi }}</b></p>
                                                <p><b>Tipe berkas:</b> {{ $posisi->tipefile }}</p>
                                                <p><b>Tanggal dibuat:</b> {{ $posisi->created_at }}</p>
                                                <p><b>Tanggal diperbaharui:</b> {{ $posisi->updated_at }}</p>
                                                <p><b>Pemilik berkas:</b> {{ $posisi->username }}</p>
                                                <br>
                                                <a class="btn btn-info" href="{{ URL::to('/berkas/'.$posisi->path) }}"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Unduh</a>
                                            </div>
                                        </td>
                                        @if(Auth::user()->id == 1)
                                            <td>
                                                <div class="col-xs-3" style="padding: 0">
                                                {!! Form::model($posisi, ['method' => 'PATCH', 'action' => ['FileController@update', $posisi->id]]) !!}
                                                <button type="submit" class="btn btn-success fa fa-check-square-o"></button>
                                                {!! Form::close() !!}
                                                </div>
                                                <div class="col-xs-3" style="padding: 0">
                                                {!! Form::model($posisi, ['method' => 'DELETE', 'action' => ['FileController@destroy', $posisi->id]]) !!}
                                                <button type="submit" class="btn btn-danger fa fa-trash"></button>
                                                {!! Form::close() !!}
                                                </div>
                                            </td>                                            
                                        @else
                                            <td>
                                                <p>Modifikasi Tidak Diizinkan</p>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>            
                    </div>
                </div>
                @else
                <div class="panel panel-default">
                    <h4 class="center">Maaf Data Tidak Ditemukan</h4>
                </div>
                @endif
            </div>
        </section>
        @include('partial.footer')
    </section>
</section>
@endsection