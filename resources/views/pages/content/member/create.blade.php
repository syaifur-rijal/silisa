@extends('layouts.app')

@section('title')
<title>Menambah Anggota</title>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar')

    <section id="main-content">
        <section class="wrapper">
            <div class="form-w3layouts">
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">Formulir Tambah Anggota</header>
                            <div class="panel-body">
                                {!! Form::open(['url' => '/member', 'class' => 'form-horizontal bucket-form', 'role' => 'form']) !!}
                                    <div class="form-group">
                                        {!! Form::label('nama', 'Nama Unit Kerja', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-6">
                                            {!! Form::text('nama', null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email', 'E-mail', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-6">
                                            {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('password', 'Kata Sandi', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-6">
                                            {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('deskripsi', 'Deskripsi', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-6">
                                            {!! Form::textarea('deskripsi', null, ['class' => 'form-control', 'rows' => '7']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('level', 'Hak Akses', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-6">
                                            {!! Form::select('level', ['admin' => 'Administrator', 'member' => 'Anggota'], null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="button-group">
                                        <div class="col-sm-3 clearfix"></div>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                                        </div>
                                    </div>                                
                                {!! Form::close() !!}
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
        @include('partial.footer')
    </section>
</section>
@endsection