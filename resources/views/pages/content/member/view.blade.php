@extends('layouts.app')

@section('title')
<title>Pengaturan Anggota</title>
@endsection

@section('content')
<section id="container">
    @include('partial.header', [
        'hasil' => $hasil, 
        'cGambar' => $cGambar,
        'cBooklet' => $cBooklet,
        'cSlide' => $cSlide,
        'cInfografis' => $cInfografis,
        'cVideo' => $cVideo,
        'cLain' => $cLain
    ])

    @include('partial.sidebar', ['class' => $class])

    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                <div class="box-add clearfix">
                    <a href="{{ url("/member/create") }}" class="pull-left btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Tambah Anggota</a>
                </div>
                @if(!empty($anggota))
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Anggota</div>
                    <div>
                        <table class="table" ui-jq="footable" ui-options='{"paging": { "enabled": true },"filtering": { "enabled": true },"sorting": { "enabled": true }}'>
                            <thead>
                                <tr>
                                    <th data-breakpoints="xs">ID</th>
                                    <th>Nama Unit</th>
                                    <th>Email</th>
                                    <th data-breakpoints="xs">Level</th>
                                    <th data-breakpoints="xs sm md">Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($anggota as $index => $posisi)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $posisi->nama }}</td>
                                        <td>{{ $posisi->email }}</td>
                                        <td>{{ $posisi->level }}</td>
                                        @if($posisi->level == 'admin' && $posisi->id == 1)
                                            <td>
                                                <p>Tidak Diizinkan</p>
                                            </td>
                                        @else
                                            <td>
                                                {!! Form::model($posisi, ['method' => 'DELETE', 'action' => ['MemberController@destroy', $posisi->id]]) !!}
                                                <a href="{{ action('MemberController@edit', $posisi->id) }}" class="btn btn-warning fa fa-pencil-square-o"></a>
                                                <button type="submit" class="btn btn-danger fa fa-trash"></button>
                                                {!! Form::close() !!}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>            
                    </div>
                </div>
                @else
                <div class="panel panel-default">
                    <h4 class="center">Maaf Data Tidak Ditemukan</h4>
                </div>
                @endif
            </div>
        </section>
        @include('partial.footer')
    </section>
</section>
@endsection