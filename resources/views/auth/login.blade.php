@extends('layouts.app')

@section('title')
<title>Halaman Otentikasi</title>
@endsection

@section('content')
<div class="log-w3">
    <div class="w3layouts-main">
        <h2>Silahkan Masuk</h2>
        {!! Form::open(['url' => '/login', 'class' => 'validate-form', 'role' => 'form']) !!}
            {!! Form::email('email', null, ['class' => 'ggg', 'placeholder' => 'Alamat Email']) !!}
            {!! Form::input('password', 'password', null, ['class' => 'ggg', 'placeholder' => 'Kata Sandi']) !!}
            <div class="clearfix">
            @if($errors->has('email') or $errors->has('password'))
                    <small style="color:white">Email atau password tidak sesuai !</small>
            @endif
            </div>
            <span><input type="checkbox" />Ingat Saya</span>
            <h6><a href="#">Lupa Password ?</a></h6>
            <div class="clearfix"></div>
            {!! Form::submit('Masuk', ['name' => 'login']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection