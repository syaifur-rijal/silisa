<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                @if($class == 'Dashboard')
                <li>
                    <a class="active" href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/repository') }}">
                        <i class="fa fa-book"></i>
                        <span>Berkas Saya</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-bullhorn"></i>
                        <span>Repositori</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/picture') }}">Gambar</a></li>
                        <li><a href="{{ url('/booklet') }}">Booklet</a></li>
                        <li><a href="{{ url('/slide') }}">Paparan</a></li>
                        <li><a href="{{ url('/infografis') }}">Infografis</a></li>
                        <li><a href="{{ url('/video') }}">Video</a></li>
                        <li><a href="{{ url('/others') }}">Lainnya</a></li>
                    </ul>
                </li>

                @if(Auth::user()->level == 'admin')
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-cog"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/member') }}">Pengaturan Anggota</a></li>
                        <li><a href="{{ url('/file') }}">Pengaturan Berkas</a></li>
                    </ul>
                </li>
                @endif

                @elseif($class == 'Berkasku')
                <li>
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="active" href="{{ url('/repository') }}">
                        <i class="fa fa-book"></i>
                        <span>Berkas Saya</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-bullhorn"></i>
                        <span>Repositori</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/picture') }}">Gambar</a></li>
                        <li><a href="{{ url('/booklet') }}">Booklet</a></li>
                        <li><a href="{{ url('/slide') }}">Paparan</a></li>
                        <li><a href="{{ url('/infografis') }}">Infografis</a></li>
                        <li><a href="{{ url('/video') }}">Video</a></li>
                        <li><a href="{{ url('/others') }}">Lainnya</a></li>
                    </ul>
                </li>
                @if(Auth::user()->level == 'admin')
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-cog"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/member') }}">Pengaturan Anggota</a></li>
                        <li><a href="{{ url('/file') }}">Pengaturan Berkas</a></li>
                    </ul>
                </li>
                @endif
                @elseif($class == 'Gambar' || $class == 'Booklet' || $class == 'Slide' || $class == 'Infografis' || $class == 'Video' || $class == 'Others')
                <li>
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/repository') }}">
                        <i class="fa fa-book"></i>
                        <span>Berkas Saya</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="active" href="javascript:;">
                        <i class="fa fa-bullhorn"></i>
                        <span>Repositori</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/picture') }}">Gambar</a></li>
                        <li><a href="{{ url('/booklet') }}">Booklet</a></li>
                        <li><a href="{{ url('/slide') }}">Paparan</a></li>
                        <li><a href="{{ url('/infografis') }}">Infografis</a></li>
                        <li><a href="{{ url('/video') }}">Video</a></li>
                        <li><a href="{{ url('/others') }}">Lainnya</a></li>
                    </ul>
                </li>
                @if(Auth::user()->level == 'admin')
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-cog"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/member') }}">Pengaturan Anggota</a></li>
                        <li><a href="{{ url('/file') }}">Pengaturan Berkas</a></li>
                    </ul>
                </li>
                @endif
                @elseif($class == 'Member' || $class == 'EditBerkas')
                <li>
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/repository') }}">
                        <i class="fa fa-book"></i>
                        <span>Berkas Saya</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-bullhorn"></i>
                        <span>Repositori</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/picture') }}">Gambar</a></li>
                        <li><a href="{{ url('/booklet') }}">Booklet</a></li>
                        <li><a href="{{ url('/slide') }}">Paparan</a></li>
                        <li><a href="{{ url('/infografis') }}">Infografis</a></li>
                        <li><a href="{{ url('/video') }}">Video</a></li>
                        <li><a href="{{ url('/others') }}">Lainnya</a></li>
                    </ul>
                </li>
                @if(Auth::user()->level == 'admin')
                <li class="sub-menu">
                    <a class="active" href="javascript:;">
                        <i class="fa fa-cog"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url('/member') }}">Pengaturan Anggota</a></li>
                        <li><a href="{{ url('/file') }}">Pengaturan Berkas</a></li>
                    </ul>
                </li>
                @endif
                @endif
            </ul>            
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->