<!--header start-->
<header class="header fixed-top clearfix">
    <!--logo start-->
    <div class="brand">
        <a href="index.html" class="logo">
            MYAPP
        </a>
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars"></div>
        </div>
    </div>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">
        @if(!empty($hasil))
        <ul class="nav top-menu">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="fa fa-trophy"></i>&nbsp;&nbsp;Poin Anda
                    <span class="badge bg-success">{{ $hasil }}</span>
                </a>
                <ul class="dropdown-menu extended tasks-bar">
                    <li><p class="">Jumlah Konten Anda</p></li>
                    <li>
                        <a href="{{ url('/picture') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Gambar</h5>
                                    <p>{{ $cGambar }} Konten</p>                                
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="45">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/booklet') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Booklet</h5>
                                    <p>{{ $cBooklet }} Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="78">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/slide') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Slide/Paparan</h5>
                                    <p>{{ $cSlide }} Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="60">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/infografis') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Infografis</h5>
                                    <p>{{ $cInfografis }} Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/video') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Video</h5>
                                    <p>{{ $cVideo }} Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/others') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Lainnya</h5>
                                    <p>{{ $cLain }} Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        @else
        <ul class="nav top-menu">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="fa fa-trophy"></i>&nbsp;&nbsp;Poin Anda
                    <span class="badge bg-success">{{ $hasil }}</span>
                </a>
                <ul class="dropdown-menu extended tasks-bar">
                    <li><p class="">Jumlah Konten Anda</p></li>
                    <li>
                        <a href="{{ url('/picture') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Gambar</h5>
                                    <p>0 Konten</p>                                
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="45">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/booklet') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Booklet</h5>
                                    <p>0 Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="78">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/slide') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Slide/Paparan</h5>
                                    <p>0 Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="60">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/infografis') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Infografis</h5>
                                    <p>0 Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/video') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Video</h5>
                                    <p>0 Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/others') }}">
                            <div class="task-info clearfix">
                                <div class="desc pull-left">
                                    <h5>Lainnya</h5>
                                    <p>0 Konten</p>
                                </div>
                                <span class="notification-pie-chart pull-right" data-percent="90">
                                    <span class="percent"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        @endif
    </div>
    <div class="top-nav clearfix">
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">
            <!-- <li>
                <input type="text" class="form-control search" placeholder="Pencarian">
            </li> -->
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <img alt="" src="{{ url('/images/2.png') }}">
                    <span class="username">{{ Auth::user()->nama }}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout">
                    <!-- <li><a href="{{ url('/profile') }}"><i class="fa fa-cogs"></i> Pengaturan Profil</a></li> -->
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i> Keluar</a></li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!--search & user info end-->
    </div>
</header>
<!--header end-->